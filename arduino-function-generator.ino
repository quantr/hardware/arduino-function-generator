/* oled */
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#define SCREEN_WIDTH 128  // OLED display width, in pixels
#define SCREEN_HEIGHT 64  // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
// The pins for I2C are defined by the Wire-library.
// On an arduino UNO:       A4(SDA), A5(SCL)
// On an arduino MEGA 2560: 20(SDA), 21(SCL)
// On an arduino LEONARDO:   2(SDA),  3(SCL), ...
#define OLED_RESET -1        // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3c  ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
/* end oled */

#include "AD9833.h"
#define CLK 6
#define DT 7
#define SW 5

AD9833 AD;

int hertz = 100;
int currentStateCLK;
int lastStateCLK;
String currentDir = "";
unsigned long lastButtonPress = 0;
int type = 0;
int types[] = { AD9833_SQUARE1, AD9833_SQUARE2, AD9833_SINE, AD9833_TRIANGLE };
int buttonType = 0;

void initOled() {
  if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ;  // Don't proceed, loop forever
  }
  display.clearDisplay();

  display.setTextSize(2);
  display.setCursor(20, 0);
  display.setTextColor(SSD1306_WHITE);
  display.println(F("Wave Gen"));

  display.setTextSize(3);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(13, 24);
  display.println(F("Quantr"));
  display.display();
  Serial.println("init 1");
  delay(1000);

  displayControls();
}

void displayControls() {
  display.clearDisplay();

  display.setTextSize(1);
  display.setCursor(5, 5);
  display.setTextColor(SSD1306_WHITE);
  display.println(F("+1"));

  display.setTextSize(1);
  display.setCursor(25, 5);
  display.setTextColor(SSD1306_WHITE);
  display.println(F("+10"));

  display.setTextSize(1);
  display.setCursor(55, 5);
  display.setTextColor(SSD1306_WHITE);
  display.println(F("+100"));

  display.setTextSize(1);
  display.setCursor(100, 5);
  display.setTextColor(SSD1306_WHITE);
  if (type == 0) {
    display.println(F("SQU"));
  } else if (type == 1) {
    display.println(F("SQU1"));
  } else if (type == 2) {
    display.println(F("SINE"));
  } else if (type == 3) {
    display.println(F("TRI"));
  }

  if (buttonType == 0) {
    display.drawLine(5, 15, 20, 15, SSD1306_WHITE);
  } else if (buttonType == 1) {
    display.drawLine(25, 15, 45, 15, SSD1306_WHITE);
  } else if (buttonType == 2) {
    display.drawLine(55, 15, 80, 15, SSD1306_WHITE);
  } else if (buttonType == 3) {
    display.drawLine(100, 15, 140, 15, SSD1306_WHITE);
  }

  display.setTextSize(3);
  if (hertz < 10000) {
    display.setCursor(15, 25);
  } else {
    display.setCursor(0, 25);
  }
  display.setTextColor(SSD1306_WHITE);
  display.print(hertz);
  display.println("Hz");

  display.display();
}

void setup() {
  // Set encoder pins as inputs
  pinMode(CLK, INPUT);
  pinMode(DT, INPUT);
  pinMode(SW, INPUT_PULLUP);

  // Setup Serial Monitor
  Serial.begin(9600);

  // Read the initial state of CLK
  lastStateCLK = digitalRead(CLK);

  AD.begin(10);
  AD.setWave(types[type]);
  AD.setFrequency(hertz, 0);

  initOled();
}

void loop() {

  // Read the current state of CLK
  currentStateCLK = digitalRead(CLK);

  // If last and current state of CLK are different, then pulse occurred
  // React to only 1 state change to avoid double count
  if (currentStateCLK != lastStateCLK && currentStateCLK == 1) {
    // If the DT state is different than the CLK state then
    // the encoder is rotating CCW so decrement
    if (buttonType == 3) {
      if (digitalRead(DT) != currentStateCLK) {
        type = (type + 1) % 4;
      } else {
        type--;
        if (type < 0) {
          type = 3;
        }
      }
      displayControls();
      AD.setWave(types[type]);
    } else {
      if (digitalRead(DT) != currentStateCLK) {
        if (buttonType == 0) {
          hertz++;
        } else if (buttonType == 1) {
          hertz += 10;
        } else if (buttonType == 2) {
          hertz += 100;
        }
        currentDir = "CW";
      } else {
        // Encoder is rotating CW so increment
        if (buttonType == 0) {
          hertz--;
        } else if (buttonType == 1) {
          hertz -= 10;
        } else if (buttonType == 2) {
          hertz -= 100;
        }
        if (hertz < 0) {
          hertz = 0;
        }
        currentDir = "CCW";
      }
      displayControls();

      Serial.print("Direction: ");
      Serial.print(currentDir);
      Serial.print(" | hertz: ");
      Serial.println(hertz);

      AD.setFrequency(hertz, 0);
    }
  }

  // Remember last CLK state
  lastStateCLK = currentStateCLK;

  // Read the button state
  int btnState = digitalRead(SW);

  //If we detect LOW signal, button is pressed
  if (btnState == LOW) {
    //if 50ms have passed since last LOW pulse, it means that the
    //button has been pressed, released and pressed again
    if (millis() - lastButtonPress > 50) {
      Serial.println("Button pressed!");
      AD.setFrequency(hertz, 0);
      buttonType = (buttonType + 1) % 4;
      displayControls();
    }


    // Remember last button press event
    lastButtonPress = millis();
  }

  // Put in a slight delay to help debounce the reading
  delay(1);
}